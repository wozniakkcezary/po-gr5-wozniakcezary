package pl.edu.uwm.wmii.wozniakcezary.laboratorium02;

import java.util.Scanner;
import java.util.Random;
public class Zadanie3 {
    public static void main(String [] Args){
        Scanner scr = new Scanner(System.in);
        int m = scr.nextInt();
        int n = scr.nextInt();
        int k = scr.nextInt();
        Random ran = new Random();
        int [][]mac = new int [m][n];
        int [][]mat = new int [n][k];
        int [][]res = new int [m][k];
        for(int i = 0 ; i <m; i++) {
            for (int j = 0; j < n; j++)
                mac[i][j] = (ran.nextInt(10) + 1);
        }
        for(int i = 0 ; i <n; i++) {
            for (int j = 0; j < k; j++)
                mat[i][j] = (ran.nextInt(10) + 1);
        }
        for(int i =0 ; i< m;i++)
        {
            for(int j=0; j < k ; j++)
            {
                int tmp =0;
                for(int li = 0; li< n ; li++)
                {
                    tmp += mac[i][li] *mat[li][j];
                }
                res[i][j] = tmp;
            }
        }
        wyp(mac);
        System.out.println("");
        wyp(mat);
        System.out.println("");
        wyp(res);
    }
    private static void wyp(int [][]tab){
        for(int i = 0; i < tab.length; i++)
        {
            for(int j= 0 ; j< tab[i].length; j++)
                System.out.print(tab[i][j] + " ");
            System.out.println("");
        }
    }
}
package pl.edu.uwm.wmii.wozniakcezary.laboratorium02;

import java.util.Random;
import java.util.Scanner;

public class Zadanie2b {

    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);

        System.out.print("Podaj n: ");
        int n = in.nextInt();
        int[] tablica;
        tablica = new int [n];

        generuj(tablica,n,-999,999);

        System.out.print("dodatnie: "+ileDodatnich(tablica)+"\n");
        System.out.print("ujemne: "+ileUjemnych(tablica)+"\n");
        System.out.print("zera: "+ileZerowych(tablica)+"\n");

    }
    public static void generuj (int tablica[], int n, int minWartosc, int maxWartosc){
        Random r = new Random();
        for (int j = 0; j < n; ++j) {
            tablica[j] = r.nextInt(maxWartosc-minWartosc)+minWartosc;
        }
    }
    public static int ileDodatnich (int tablica[]){
        int dodatnie=0;
        for(int i=0;i<tablica.length;i++){
            if(tablica[i]>0) dodatnie++;
        }
        return dodatnie;
    }
    public static int ileUjemnych (int tablica[]){
        int ujemne=0;
        for(int i=0;i<tablica.length;i++){
            if(tablica[i]<0) ujemne++;
        }
        return ujemne;
    }
    public static int ileZerowych (int tablica[]){
        int zera=0;
        for(int i=0;i<tablica.length;i++){
            if(tablica[i]==0) zera++;
        }
        return zera;
    }
}
package pl.edu.uwm.wmii.wozniakcezary.laboratorium02;

import java.util.Random;
import java.util.Scanner;

public class Zadanie1{
    public static void main(String[] args){
        Scanner scan = new Scanner(System.in);
        int n = 0;
        while (1 > n || n > 100)
        {
            System.out.println("Podaj ilosc liczb w tablicy: ");
            n = scan.nextInt();
        }

        int[] tablica = new int[n];
        for (int i = 0; i < n; i++)
        {
            int liczba = getRandomNumberInRange(-999, 999);
            tablica[i] = liczba;
        }


        // a)
        int parzyste = 0;
        int nieparzyste = 0;
        for (int i = 0; i < n; i++)
        {
            if (tablica[i] % 2 == 0)
                parzyste++;
            else
                nieparzyste++;
        }

        System.out.println("Ilosc liczb parzystych: " + parzyste);
        System.out.println("Ilosc liczb nieparzystych: " + nieparzyste);

        // b)
        int zerowe = 0;
        int ujemne = 0;
        int dodatnie = 0;
        for (int i = 0; i < n; i++)
        {
            if (tablica[i] > 0)
                dodatnie++;
            else if (tablica[i] < 0)
                ujemne++;
            else
                zerowe++;
        }

        System.out.println("Ilosc liczb zerowych: " + zerowe);
        System.out.println("Ilosc liczb ujemnych: " + ujemne);
        System.out.println("Ilosc liczb dodatnich: " + dodatnie);

        // c)
        int max = tablica[0];
        for (int i = 1; i < n; i++)
        {
            if (tablica[i] > max)
                tablica[i] = max;
        }

        int iloscWystapienMax = 0;
        for (int i = 0; i < n; i++)
        {
            if (tablica[i] == max)
                iloscWystapienMax++;
        }

        System.out.println("Najwiekszy element w tablicy: " + max);
        System.out.println("Ilosc wystapien najwiekszego elementu w tablicy: " + iloscWystapienMax);

        // d)
        int sumaUjemnych = 0;
        int sumaDodatnich = 0;
        for (int i = 0; i < n; i++)
        {
            if(tablica[i] > 0)
                sumaDodatnich = sumaDodatnich + tablica[i];
            else
                sumaUjemnych = sumaUjemnych + tablica[i];
        }

        System.out.println("Suma liczb ujemnych: " + sumaUjemnych);
        System.out.println("Suma liczb dodatnich: " + sumaDodatnich);

        // e)
        int najdluzszyOdcinekDodatnich = 0;
        int buffer = 0;
        for (int i = 0; i < n; i++)
        {
            if (tablica[i] >= 0)
                buffer++;
            else
                buffer = 0;

            if (buffer > najdluzszyOdcinekDodatnich)
                najdluzszyOdcinekDodatnich = buffer;
        }

        System.out.println("Najdluzszy fragment tablicy, w ktorym wszystkie elementy sa dodatnie ma dlugosc: " + najdluzszyOdcinekDodatnich);

        // f)
        for (int i = 0; i < n; i++)
        {
            if (tablica[i] >= 0)
                tablica[i] = 1;
            else
                tablica[i] = -1;
        }
        Wypisz(tablica);

        // g)
        System.out.println("Wczytaj liczbe do zmiennej lewy: ");
        int lewy = scan.nextInt();
        System.out.println("Wczytaj liczbe do zmiennej prawy: ");
        int prawy = scan.nextInt();

        for (int i = lewy-1 ; i < prawy / 2 ; i++)
        {
            int temp = tablica[i];
            tablica[i] = tablica[prawy - i - 1];
            tablica[prawy - i - 1] = temp;
        }
        Wypisz(tablica);


    }

public static void Wypisz(int[] tab)
{
    for (int el : tab)
    {
        System.out.println(el + " ");
    }
    System.out.println("");
}

    private static int getRandomNumberInRange(int min, int max) {

        if (min >= max) {
            throw new IllegalArgumentException("max must be greater than min");
        }

        Random r = new Random();
        return r.nextInt((max - min) + 1) + min;
    }
}
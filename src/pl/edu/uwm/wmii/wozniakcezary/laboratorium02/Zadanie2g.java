package pl.edu.uwm.wmii.wozniakcezary.laboratorium02;

import java.util.Random;
import java.util.Scanner;

public class Zadanie2g {

    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);

        System.out.print("Podaj n: ");
        int n = in.nextInt();
        int[] tablica;
        tablica = new int [n];

        generuj(tablica,n,-999,999);

        System.out.print("Podaj lewy (indeks tablicy): ");
        int lewy = in.nextInt();
        System.out.print("Podaj prawy (indeks tablicy): ");
        int prawy = in.nextInt();

        System.out.print("\nPrzed odwroceniem: \n");
        wypisz(tablica);
        System.out.print("\n");

        odwrocFragment(tablica,lewy,prawy);

        System.out.print("\nPo odwroceniu: \n");
        wypisz(tablica);
        System.out.print("\n");
    }
    public static void generuj (int tablica[], int n, int minWartosc, int maxWartosc){
        Random r = new Random();
        for (int j = 0; j < n; ++j) {
            tablica[j] = r.nextInt(maxWartosc-minWartosc)+minWartosc;
        }
    }
    public static void odwrocFragment(int tablica[], int lewy, int prawy){
        int temp;
        for(int i=lewy;i<prawy-((prawy-lewy)/2);i++){
            temp=tablica[i];
            tablica[i]=tablica[prawy-(i-lewy)];
            tablica[prawy-(i-lewy)]=temp;
        }
    }
    public static void wypisz(int[] tab) {
        for (int el : tab) {
            System.out.print(el + " ");
        }
        System.out.println("");
    }
}
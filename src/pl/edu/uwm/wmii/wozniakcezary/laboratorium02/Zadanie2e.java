package pl.edu.uwm.wmii.wozniakcezary.laboratorium02;

import java.util.Random;
import java.util.Scanner;

public class Zadanie2e {

    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);

        System.out.print("Podaj n: ");
        int n = in.nextInt();
        int[] tablica;
        tablica = new int [n];

        generuj(tablica,n,-999,999);



        System.out.print("najdluzszy fragment: "+dlugoscMaksymalnegoCiaguDodatnich(tablica)+"\n");

    }
    public static void generuj (int tablica[], int n, int minWartosc, int maxWartosc){
        Random r = new Random();
        for (int j = 0; j < n; ++j) {
            tablica[j] = r.nextInt(maxWartosc-minWartosc)+minWartosc;
        }
    }
    public static int dlugoscMaksymalnegoCiaguDodatnich (int tablica[]){
        int najdluzszyFragment=0, aktualnyFragment=0;
        boolean czyPoprzedniDodatni=true;
        for(int i=0;i<tablica.length;i++){
            if(tablica[i]>0){
                if(czyPoprzedniDodatni)aktualnyFragment++;
                else aktualnyFragment=1;
                czyPoprzedniDodatni=true;
            }
            else{
                czyPoprzedniDodatni=false;
                aktualnyFragment=0;
            }
            if(aktualnyFragment>najdluzszyFragment)najdluzszyFragment=aktualnyFragment;
        }
        return najdluzszyFragment;
    }
}
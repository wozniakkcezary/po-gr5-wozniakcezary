package pl.edu.uwm.wmii.wozniakcezary.laboratorium02;

import java.util.Random;
import java.util.Scanner;

public class Zadanie2c {

    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);

        System.out.print("Podaj n: ");
        int n = in.nextInt();
        int[] tablica;
        tablica = new int [n];

        generuj(tablica,n,-999,999);


        System.out.print("ile razy wystepuje najwiekszy element: "+ileMaksymalnych(tablica)+"\n");

    }
    public static void generuj (int tablica[], int n, int minWartosc, int maxWartosc){
        Random r = new Random();
        for (int j = 0; j < n; ++j) {
            tablica[j] = r.nextInt(maxWartosc-minWartosc)+minWartosc;
        }
    }
    public static int ileMaksymalnych (int tablica[]){
        int najwiekszy=tablica[0], ileRazy=1;
        for(int i=1;i<tablica.length;i++){
            if(tablica[i]>najwiekszy){
                najwiekszy=tablica[i];
                ileRazy=1;
            }
            else if(tablica[i]==najwiekszy) ileRazy++;
        }
        return ileRazy;
    }
}
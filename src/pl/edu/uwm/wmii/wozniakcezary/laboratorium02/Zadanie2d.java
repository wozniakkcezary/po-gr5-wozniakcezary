package pl.edu.uwm.wmii.wozniakcezary.laboratorium02;

import java.util.Random;
import java.util.Scanner;

public class Zadanie2d {

    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);

        System.out.print("Podaj n: ");
        int n = in.nextInt();
        int[] tablica;
        tablica = new int [n];

        generuj(tablica,n,-999,999);

        System.out.print("suma dodatnich: "+sumaDodatnich(tablica)+"\n");
        System.out.print("suma ujemnych: "+sumaUjemnych(tablica)+"\n");

    }
    public static void generuj (int tablica[], int n, int minWartosc, int maxWartosc){
        Random r = new Random();
        for (int j = 0; j < n; ++j) {
            tablica[j] = r.nextInt(maxWartosc-minWartosc)+minWartosc;
        }
    }
    public static int sumaDodatnich (int tablica[]){
        int suma=0;
        for(int i=0;i<tablica.length;i++){
            if(tablica[i]>0) suma+=tablica[i];
        }
        return suma;
    }
    public static int sumaUjemnych (int tablica[]){
        int suma=0;
        for(int i=0;i<tablica.length;i++){
            if(tablica[i]<0) suma+=tablica[i];
        }
        return suma;
    }
}
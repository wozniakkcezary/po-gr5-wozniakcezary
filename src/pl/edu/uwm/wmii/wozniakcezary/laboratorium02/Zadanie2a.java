package pl.edu.uwm.wmii.wozniakcezary.laboratorium02;

import java.util.Random;
import java.util.Scanner;

public class Zadanie2a {

    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);

        System.out.print("Podaj n: ");
        int n = in.nextInt();
        int[] tablica;
        tablica = new int [n];

        generuj(tablica,n,-999,999);


        System.out.print("parzyste: "+ileParzystych(tablica)+"\n");
        System.out.print("nieparzyste: "+ileNieparzystych(tablica)+"\n");

    }
    public static void generuj (int tablica[], int n, int minWartosc, int maxWartosc){
        Random r = new Random();
        for (int j = 0; j < n; ++j) {
            tablica[j] = r.nextInt(maxWartosc-minWartosc)+minWartosc;
        }
    }
    public static int ileParzystych (int tablica[]){
        int parzyste=0;
        for(int i=0;i<tablica.length;i++){
            if(tablica[i]%2==0) parzyste++;
        }
        return parzyste;
    }
    public static int ileNieparzystych (int tablica[]){
        return tablica.length-ileParzystych(tablica);
    }
}
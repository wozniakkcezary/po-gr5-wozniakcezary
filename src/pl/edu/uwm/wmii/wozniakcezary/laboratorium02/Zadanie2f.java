package pl.edu.uwm.wmii.wozniakcezary.laboratorium02;

import java.util.Random;
import java.util.Scanner;

public class Zadanie2f {

    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);

        System.out.print("Podaj n: ");
        int n = in.nextInt();
        int[] tablica;
        tablica = new int [n];

        generuj(tablica,n,-999,999);

        signum(tablica);

        wypisz(tablica);

    }
    public static void generuj (int tablica[], int n, int minWartosc, int maxWartosc){
        Random r = new Random();
        for (int j = 0; j < n; ++j) {
            tablica[j] = r.nextInt(maxWartosc-minWartosc)+minWartosc;
        }
    }
    public static void signum(int tablica[]){
        for(int i=0;i<tablica.length;i++){
            if(tablica[i]<0) tablica[i]=-1;
            else if(tablica[i]>0) tablica[i]=1;
        }
    }
    public static void wypisz(int[] tab) {
        for (int el : tab) {
            System.out.print(el + " ");
        }
        System.out.println("");
    }
}
package pl.edu.uwm.wmii.wozniakcezary.laboratorium08;

import java.time.LocalDate;

public class Osoba implements Comparable {
    @Override
    public int compareTo(Object o) {
        if(nazwisko.equals(((Osoba)o).nazwisko)) return 1;
        if(dataUrodzenia ==  ((Osoba) o).dataUrodzenia) return 1;
        return 0;
    }
    @Override
    public String toString(){
        return this.getClass().getSimpleName() + " [" + nazwisko + dataUrodzenia + "]";
    }

    public String getNazwisko() {
        return nazwisko;
    }

    @Override
    public boolean equals(Object obj) {
        if(this == obj) return true;
        if(obj == null) return false;
        if(getClass() != obj.getClass()) return false;

        Osoba oso = (Osoba) obj;

        return nazwisko.equals(oso.nazwisko)
                && dataUrodzenia == oso.dataUrodzenia;
    }

    public void setNazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
    }

    public LocalDate getDataUrodzenia() {
        return dataUrodzenia;
    }

    public void setDataUrodzenia(LocalDate dataUrodzenia) {
        this.dataUrodzenia = dataUrodzenia;
    }

    private String nazwisko;
    private LocalDate dataUrodzenia;

}

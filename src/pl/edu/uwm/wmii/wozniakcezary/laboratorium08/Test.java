package pl.edu.uwm.wmii.wozniakcezary.laboratorium08;

import java.time.LocalDate;

public class Test {
    public static void main(String[] args){
        Osoba os = new Osoba();
        os.setNazwisko("Kaczmarczyk");
        os.setDataUrodzenia(LocalDate.parse("2018-12-23"));
        System.out.println(os.toString());
    }
}

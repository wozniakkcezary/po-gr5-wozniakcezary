package pl.edu.uwm.wmii.wozniakcezary.laboratorium07;

import java.util.ArrayList;
public class TestInstrumenty {
    public static void main(String[] args){
        ArrayList<Instrument>lista = new ArrayList<>();
        
        lista.add(new Flet());
        lista.add(new Skrzypce());
        lista.add(new Skrzypce());
        lista.add(new Fortepian());
        lista.add(new Fortepian());

        for (Instrument item :
                lista) {
            item.dzwiek();
        }
    }
}

package pl.edu.uwm.wmii.wozniakcezary.laboratorium07;

import java.time.LocalDate;

class Pracownik extends Osoba
{
    public Pracownik(String nazwisko, String[] imiona, boolean plec, LocalDate dataUrodzenia, LocalDate dataZatrudnienia, double pobory)
    {
        super(nazwisko, imiona, plec, dataUrodzenia);
        this.pobory = pobory;
        this.dataZatrudnienia = dataZatrudnienia;
    }

    public double getPobory()
    {
        return pobory;
    }

    public String getOpis()
    {
        return String.format("pracownik z pensją %.2f zł", pobory);
    }

    public LocalDate getDataZatrudnienia() {
        return dataZatrudnienia;
    }

    private double pobory;
    private LocalDate dataZatrudnienia;
}

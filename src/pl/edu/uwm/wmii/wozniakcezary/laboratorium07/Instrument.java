package pl.edu.uwm.wmii.wozniakcezary.laboratorium07;

import java.time.LocalDate;

public abstract class Instrument {
    public abstract void dzwiek();

    @Override
    public boolean equals(Object o){
        if(producent.equals(((Instrument) o).producent)) return true;
        return false;
    }
    @Override
    public String toString(){
        return producent + " rokProdukcji : " + rokProdukcji;
    }

    public String getProducent() {
        return producent;
    }

    public LocalDate getRokProdukcji() {
        return rokProdukcji;
    }

    private String producent;
    private LocalDate rokProdukcji;

}

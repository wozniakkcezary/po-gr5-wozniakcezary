package pl.edu.uwm.wmii.wozniakcezary.laboratorium07;

import java.time.LocalDate;

class Student extends Osoba
{
    public Student(String nazwisko, String[] imiona, boolean plec, LocalDate dataUrodzenia, String kierunek, double sredniaOcen)
    {
        super(nazwisko, imiona, plec, dataUrodzenia);
        this.kierunek = kierunek;
        this.sredniaOcen = sredniaOcen;
    }

    public String getOpis()
    {
        return "kierunek studiów: " + kierunek;
    }

    public double getSredniaOcen() {
        return sredniaOcen;
    }

    public void setSredniaOcen(double sredniaOcen) {
        this.sredniaOcen = sredniaOcen;
    }

    private String kierunek;
    private double sredniaOcen;
}

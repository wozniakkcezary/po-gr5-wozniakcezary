package pl.edu.uwm.wmii.wozniakcezary.laboratorium07;

import java.time.LocalDate;
import java.util.*;

public class TestOsoba
{
    public static void main(String[] args)
    {
        Osoba[] ludzie = new Osoba[2];

        ludzie[0] = new Pracownik("Trump", new String[]{"Mark" , "Hermiona"},true, LocalDate.parse("2010-12-10"), LocalDate.parse("2015-06-04"),2334.21); // daty powinny byc w 3 intach i wykorzystac LocalDate.dateof(rok,miesiac, dzien)
        ludzie[1] = new Student(" Nowak", new String[]{"Markus", "hehe"},false,LocalDate.parse("1999-02-14"),"informatyka", 3.9);
        // ludzie[2] = new Osoba("Dyl Sowizdrzał");

        for (Osoba p : ludzie) {
            System.out.println(p.getNazwisko() + ": " + p.getOpis());
        }
    }
}



package pl.edu.uwm.wmii.wozniakcezary.laboratorium04;

import java.util.ArrayList;
import java.util.Arrays;

public class Zadanie5 {
    public static void main(String []args){
        ArrayList<Integer>puppy = new ArrayList<Integer>(Arrays.asList(12,41,45,67,612));
        System.out.println(reverse(puppy));
    }
    public static ArrayList<Integer>reverse(ArrayList<Integer>a){

        int [] tab = new int[a.size()];
        for(int i =0 ; i < a.size(); i++)
            tab[i] = a.get(a.size() - 1 -i);
        for(int i =0 ; i < a.size(); i ++)
            a.set(i,tab[i]);

        return a;
    }
}
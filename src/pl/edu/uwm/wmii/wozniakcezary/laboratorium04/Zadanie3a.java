package pl.edu.uwm.wmii.wozniakcezary.laboratorium04;

import java.util.ArrayList;
import java.util.Collections;

public class Zadanie3a {
    public static void main(String[] args) {
        ArrayList<Integer> byku = new ArrayList<Integer>();
        ArrayList<Integer> kotek = new ArrayList<Integer>();
        byku.add(1);
        byku.add(4);
        byku.add(9);
        byku.add(16);

        kotek.add(9);
        kotek.add(7);
        kotek.add(4);
        kotek.add(9);
        kotek.add(11);

        ArrayList<Integer> tmp = mergeSorted(byku, kotek);
        for (int item :
                tmp) {
            System.out.println(item);
        }
    }
    public static ArrayList<Integer>mergeSorted(ArrayList<Integer>a, ArrayList<Integer>b){
        ArrayList<Integer> res = new ArrayList<Integer>();
        res = Zadanie2.merge(a,b);
        Collections.sort(res);
        return res;

    }
}
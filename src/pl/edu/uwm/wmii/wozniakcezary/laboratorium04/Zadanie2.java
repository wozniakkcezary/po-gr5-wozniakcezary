package pl.edu.uwm.wmii.wozniakcezary.laboratorium04;

import java.util.ArrayList;

public class Zadanie2 {
    public static void main(String []args){
        ArrayList<Integer>one = new ArrayList<Integer>();
        ArrayList<Integer>two = new ArrayList<Integer>();
        one.add(12);
        one.add(345);
        one.add(678);
        one.add(421);
        two.add(9909);
        two.add(9965);
        ArrayList<Integer>tmp = new ArrayList<Integer>(merge(one,two));
        for (int i =0 ; i< tmp.size(); i ++)
            System.out.println(tmp.get(i));
    }
    public static ArrayList<Integer>merge(ArrayList<Integer>a, ArrayList<Integer>b){
        ArrayList<Integer>res = new ArrayList<Integer>();
        int length;
        boolean choice = false;
        if(a.size() > b.size()) {
            length = b.size();
            choice = true;
        }
        else
            length = a.size();
        for(int i =0 ; i <= length; i++){
            if(i == length)
            {
                if(!choice)
                    for(int j =i;  j< b.size(); j++)
                        res.add(b.get(j));
                else
                    for(int j = i ; j <a.size(); j++)
                        res.add(a.get(j));
                break;
            }
            res.add(a.get(i));
            res.add(b.get(i));
        }
        return res;
    }
}
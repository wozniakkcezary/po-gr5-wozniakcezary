package pl.edu.uwm.wmii.wozniakcezary.laboratorium04;

import java.util.ArrayList;

public class Zadanie1 {
    public static void main(String []args){
        ArrayList<Integer>b = new ArrayList<Integer>();
        b.add(1);
        b.add(4);
        b.add(9);
        b.add(16);
        ArrayList<Integer>c = new ArrayList<Integer>();
        c.add(9);
        c.add(7);
        c.add(4);
        c.add(11);
        ArrayList<Integer>tmp = new ArrayList<Integer>();
        tmp = append(b,c);
        for (int item:
                tmp) {
            System.out.println(item);
        }

    }
    public static ArrayList<Integer>append(ArrayList<Integer> a, ArrayList<Integer>b){
        ArrayList<Integer>res = new ArrayList<Integer>(a);
        res.addAll(b);
        return res;
    }
}

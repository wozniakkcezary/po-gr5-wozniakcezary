package pl.edu.uwm.wmii.wozniakcezary.laboratorium04;

import java.util.ArrayList;
import java.util.Arrays;

public class Zadanie4 {
    public static void main(String [] args){
        ArrayList<Integer>rep = new ArrayList<Integer>(Arrays.asList(1,2,3,4,5,6,7,8,9));
        ArrayList<Integer>piesek = new ArrayList<Integer>();
        piesek = reversed(rep);
        System.out.println(piesek);
    }
    public static ArrayList<Integer>reversed(ArrayList<Integer>a){
        ArrayList<Integer>res = new ArrayList<Integer>();
        for(int i=a.size() -1 ; i >= 0; i--){
            res.add(a.get(i));

        }
        return res;
    }
}
package pl.edu.uwm.wmii.wozniakcezary.przykladoweKolokwium;

import java.time.LocalDate;

public class Laptop extends Komputer implements Cloneable, Comparable<Komputer> {
    private Boolean czyApple;

    public Laptop(String nazwa, LocalDate dataProdukcji, Boolean czyApple) {
        super(nazwa, dataProdukcji);
        this.czyApple = czyApple;
    }

    public Boolean getCzyApple() {
        return czyApple;
    }

    @Override
    public int compareTo(Komputer o) {
        int porownanieKomputer = super.compareTo(o);

        if (o instanceof Laptop && porownanieKomputer == 0) {
            int roznicaCzyApple = czyApple.compareTo(((Laptop) o).czyApple);
            if (roznicaCzyApple != 0)
                return roznicaCzyApple;
        }
        return super.compareTo(o);
    }
}

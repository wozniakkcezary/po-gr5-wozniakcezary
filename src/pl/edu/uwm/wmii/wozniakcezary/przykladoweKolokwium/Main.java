package pl.edu.uwm.wmii.wozniakcezary.przykladoweKolokwium;

import java.time.LocalDate;
import java.util.ArrayList;

public class Main {

    public static void main (String[] args) {
        var grupa = new ArrayList<Komputer>();

        String nazwa1 = "Apple";
        String nazwa2 = "Asus";

        LocalDate data1 = LocalDate.of(2017, 05, 13);
        LocalDate data2 = LocalDate.of(2015, 12, 22);

        grupa.add(new Komputer(nazwa1, data1));
        grupa.add(new Komputer(nazwa1, data2));
        grupa.add(new Komputer(nazwa2, data1));
        grupa.add(new Komputer(nazwa2, data2));
        grupa.add(new Komputer(nazwa1, data2));

        for (Komputer item : grupa)
            System.out.println(item.getNazwa() + "\t" + item.getDataProdukcji());

        grupa.sort(Komputer::compareTo);
        System.out.println();

        for (Komputer item : grupa)
            System.out.println(item.getNazwa() + "\t" + item.getDataProdukcji());

        /////////////////////////////////////////////////////////////////////////

        var grupaLaptopow = new ArrayList<Laptop>();

        grupaLaptopow.add(new Laptop (nazwa1, data1, true));
        grupaLaptopow.add(new Laptop (nazwa1, data2, false));
        grupaLaptopow.add(new Laptop (nazwa2, data1, false));
        grupaLaptopow.add(new Laptop (nazwa2, data2, false));
        grupaLaptopow.add(new Laptop (nazwa1, data2, true));

        System.out.println();
        for (Laptop item : grupaLaptopow)
            System.out.println(item.getNazwa() + "\t" + item.getDataProdukcji() + "\t APPLE: " + item.getCzyApple());

        grupaLaptopow.sort(Laptop::compareTo);
        System.out.println();

        for (Laptop item : grupaLaptopow)
            System.out.println(item.getNazwa() + "\t" + item.getDataProdukcji() + "\t APPLE: " + item.getCzyApple());
    }
}

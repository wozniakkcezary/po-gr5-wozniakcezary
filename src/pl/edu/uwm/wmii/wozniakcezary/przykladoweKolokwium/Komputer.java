package pl.edu.uwm.wmii.wozniakcezary.przykladoweKolokwium;

import java.time.LocalDate;
import java.util.Objects;

public class Komputer implements Cloneable, Comparable<Komputer> {
    private String nazwa;
    private LocalDate dataProdukcji;

    public Komputer(String nazwa, LocalDate dataProdukcji) {
        this.nazwa = nazwa;
        this.dataProdukcji = dataProdukcji;
    }

    public String getNazwa() {
        return nazwa;
    }

    public LocalDate getDataProdukcji() {
        return dataProdukcji;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Komputer komputer = (Komputer) o;
        return Objects.equals(nazwa, komputer.nazwa) &&
                Objects.equals(dataProdukcji, komputer.dataProdukcji);
    }

    @Override
    public int compareTo(Komputer o) {
        if (o == null)
            throw new NullPointerException("Cannot pass null obj");

        int roznicaNazw = nazwa.compareToIgnoreCase(o.nazwa);
        if (roznicaNazw != 0)
            return roznicaNazw;

        int roznicaDatProdukcji = dataProdukcji.compareTo(o.dataProdukcji);
        if (roznicaDatProdukcji != 0)
            return roznicaDatProdukcji;

        return 0;
    }

    @Override
    protected Komputer clone() throws CloneNotSupportedException {
        return (Komputer)super.clone();
    }
}

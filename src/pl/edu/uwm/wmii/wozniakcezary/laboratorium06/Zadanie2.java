package pl.edu.uwm.wmii.wozniakcezary.laboratorium06;

public class Zadanie2 {
    public static void main(String [] args){
        Osoba os = new Osoba("Kazimieerczak", 2030);
        System.out.println(os);
        Nauczyciel na = new Nauczyciel(2999.3, "Bierut", 2991);
        System.out.println(na);
        Student st = new Student("Informatyk", "CialgeMaloKebabowicz",4432);
        System.out.println(st);
    }
}
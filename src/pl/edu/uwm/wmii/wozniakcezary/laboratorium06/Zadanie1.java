package pl.edu.uwm.wmii.wozniakcezary.laboratorium06;

public class Zadanie1 {
    public static void main(String[] args){
        NazwanyPunkt a = new NazwanyPunkt(3,5,"port");
        a.show();

        Punkt b = new Punkt(3,5);
        b.show();

        Punkt c = new NazwanyPunkt(3, 6, "tawerna");
        c.show();

        // a =b;
        // a = (NazwanyPunkt) b;
        // a = c;

    }
}
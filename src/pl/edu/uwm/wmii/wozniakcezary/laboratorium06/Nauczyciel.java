package pl.edu.uwm.wmii.wozniakcezary.laboratorium06;

public class Nauczyciel extends Osoba{
    public Nauczyciel(double pensja, String nazwisko, int rokUrodzenia){

        super(nazwisko, rokUrodzenia);
        this.pensja =pensja;

    }

    public double getPensja() {
        return pensja;
    }

    @Override
    public String toString() {
        return super.toString() +" Nauczyciel " + "pensja=" + getPensja();
    }

    private double pensja;
}
package pl.edu.uwm.wmii.wozniakcezary.laboratorium06;

public class Adres {
    public Adres(String ulica, int nrDomu, String miasto, int KodPocztowy, int nrMieszkania){
        this.ulica = ulica;
        this.nrDomu = nrDomu;
        this.miasto = miasto;
        this.KodPocztowy = KodPocztowy;
        this.nrMieszkania = nrMieszkania;
    }
    public Adres(String ulica, int nrDomu, String miasto, int KodPocztowy){
        this.ulica = ulica;
        this.nrDomu = nrDomu;
        this.miasto = miasto;
        this.KodPocztowy = KodPocztowy;
    }
    public void pokaz(){
        System.out.println(KodPocztowy + " " + miasto);
        System.out.print(ulica + " "+nrDomu);
        if(nrMieszkania > 0)
            System.out.println(" " + nrMieszkania);
    }
    public boolean przed(int kodPocztowy){
        if(kodPocztowy < this.KodPocztowy){
            return true;
        }
        return false;
    }

    private String ulica;
    private int nrDomu;
    private String miasto;
    private int KodPocztowy;
    private int nrMieszkania;

}
package pl.edu.uwm.wmii.wozniakcezary.laboratorium06;

public class Student extends Osoba{
    public Student(String kierunek, String nazwisko, int rokUrodzenia){
        super(nazwisko, rokUrodzenia);
        this.kierunek = kierunek;
    }

    public String getKierunek() {
        return kierunek;
    }

    public String toString(){
        return super.toString() + "K ieruneczek: " + getKierunek();
    }
    private String kierunek;
}
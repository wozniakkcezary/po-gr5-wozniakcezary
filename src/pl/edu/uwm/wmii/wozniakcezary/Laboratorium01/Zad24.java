package pl.edu.uwm.wmii.wozniakcezary.Laboratorium01;

import java.util.Scanner;

public class Zad24 {

    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);

        System.out.print("Podaj n: ");
        int n = in.nextInt();

        double najmniejsza=0,najwieksza=0;
        double x=0;

        for(int i=1;i<=n;i++){
            System.out.print("Podaj liczbe: ");
            x=in.nextInt();
            if(i==1){
                najmniejsza=x;
                najwieksza=x;
            }
            else if(najmniejsza>x) najmniejsza=x;
            else if(najwieksza<x) najwieksza=x;
        }

        System.out.print("\nnajwieksza:  "+najwieksza);
        System.out.print("\nnajmniejsza: "+najmniejsza);

    }
}
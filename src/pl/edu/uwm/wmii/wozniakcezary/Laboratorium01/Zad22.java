package pl.edu.uwm.wmii.wozniakcezary.Laboratorium01;

import java.util.Scanner;

public class Zad22 {

    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);

        System.out.print("Podaj n: ");
        int n = in.nextInt();

        double suma=0;
        double x=0;

        for(int i=1;i<=n;i++){
            System.out.print("Podaj liczbe: ");
            x=in.nextInt();
            if(x>0) suma+=x;
        }

        System.out.print("\npodwojona suma: "+2*suma);

    }
}
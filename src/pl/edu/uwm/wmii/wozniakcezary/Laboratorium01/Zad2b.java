package pl.edu.uwm.wmii.wozniakcezary.Laboratorium01;

import java.util.Scanner;

public class Zad2b {

    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);

        System.out.print("Podaj n: ");
        int n = in.nextInt();

        int ilosc=0;

        for(int i=0;i<n;i++){
            System.out.print("Podaj liczbe: ");
            int x=in.nextInt();
            if(x%3==0 && x%5!=0) ilosc++;

        }
        System.out.print("ilosc podzielnych przez 3, ale niepodzielnych przez 5: "+ilosc);

    }
}
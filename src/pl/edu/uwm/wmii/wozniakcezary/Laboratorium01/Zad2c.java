package pl.edu.uwm.wmii.wozniakcezary.Laboratorium01;

import java.util.Scanner;

public class Zad2c {

    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);

        System.out.print("Podaj n: ");
        int n = in.nextInt();

        int ilosc=0;

        for(int i=0;i<n;i++){
            System.out.print("Podaj liczbe: ");
            int x=in.nextInt();
            double a=Math.sqrt((double)x);

            if(a%2==0) ilosc++;

        }
        System.out.print("kwadraty liczb parzystych: "+ilosc);

    }
}
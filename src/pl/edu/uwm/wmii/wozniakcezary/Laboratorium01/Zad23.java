package pl.edu.uwm.wmii.wozniakcezary.Laboratorium01;

import java.util.Scanner;

public class Zad23 {

    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);

        System.out.print("Podaj n: ");
        int n = in.nextInt();

        int dodatnie=0,zera=0,ujemne=0;
        double x=0;

        for(int i=1;i<=n;i++){
            System.out.print("Podaj liczbe: ");
            x=in.nextInt();
            if(x>0) dodatnie++;
            else if(x<0) ujemne++;
            else zera++;
        }

        System.out.print("\ndodatnie: "+dodatnie);
        System.out.print("\nujemne:   "+ujemne);
        System.out.print("\nzera:     "+zera);

    }
}
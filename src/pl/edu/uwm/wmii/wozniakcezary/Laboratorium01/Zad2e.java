package pl.edu.uwm.wmii.wozniakcezary.Laboratorium01;

import java.util.Scanner;

public class Zad2e {

    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);

        System.out.print("Podaj n: ");
        int n = in.nextInt();

        int ilosc=0;
        int x=0;

        for(int i=0;i<n;i++){
            int silnia=1;
            for (int j = 1; j <= i+1; j++) {
                silnia *= j;
            }
            System.out.print("Podaj liczbe: ");
            x=in.nextInt();
            if(Math.pow(2,i+1)<x && x<silnia) ilosc++;

        }
        System.out.print("\nspelniajace warunek: "+ilosc);

    }
}
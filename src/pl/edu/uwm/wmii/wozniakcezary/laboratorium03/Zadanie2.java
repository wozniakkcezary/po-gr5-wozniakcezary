package pl.edu.uwm.wmii.wozniakcezary.laboratorium03;

import java.util.Scanner;
import java.io.File;
import java.io.FileNotFoundException;

public class Zadanie2 {
    public static void main(String[] args) throws FileNotFoundException{
        Scanner in = new Scanner(System.in);
        System.out.println("podaj nazwe pliku (pelen adres razem z rozszerzeniem np. C:\\JavaTest\\test.txt):");
        String str = in.nextLine();
        System.out.println("podaj char:");
        char c = in.next().charAt(0);
        System.out.println("powtarza się "+countChar(str,c)+" razy");
    }

    public static int countChar(String fileName,char c) {
        try {
            Scanner file = new Scanner(new File(fileName));
            int wynik = 0;
            while (file.hasNextLine()) {
                String line = file.nextLine();
                for (int i = 0; i < line.length(); i++) {
                    if (c==line.charAt(i)) {
                        wynik++;
                    }
                }
            }
            return wynik;
        }
        catch (FileNotFoundException ex){
            System.out.println("nie znaleziono pliku");
            return 0;
        }


    }
}

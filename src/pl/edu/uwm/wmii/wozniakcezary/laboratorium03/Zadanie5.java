package pl.edu.uwm.wmii.wozniakcezary.laboratorium03;

import java.math.RoundingMode;
import java.util.Scanner;
import java.math.BigDecimal;

public class Zadanie5 {
    public static void main(String[] args){
        Scanner in = new Scanner(System.in);
        System.out.println("podaj wartosc poczatkowa:");
        double k = in.nextDouble();
        System.out.println("podaj stope roczna w %:");
        double p = in.nextDouble();
        System.out.println("podaj ilosc lat:");
        int n = in.nextInt();
        System.out.println(account(k,p,n));

    }

    public static BigDecimal account(double k, double p, int n) {
        BigDecimal wynik = new BigDecimal(k);
        BigDecimal rate = new BigDecimal(1+(p/100));
        for(int i = 0; i<n; i++){
            wynik=wynik.multiply(rate).setScale(2, RoundingMode.HALF_DOWN);
        }
        return  wynik;
    }
}

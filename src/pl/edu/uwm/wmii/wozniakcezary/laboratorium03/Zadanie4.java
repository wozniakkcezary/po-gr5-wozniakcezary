package pl.edu.uwm.wmii.wozniakcezary.laboratorium03;

import java.util.Scanner;
import java.math.BigInteger;

public class Zadanie4 {
    public static void main(String[] args){
        Scanner in = new Scanner(System.in);
        System.out.println("podaj n:");
        int n = in.nextInt();
        System.out.println(chess(n));

    }

    public static BigInteger chess(int n){
        BigInteger wynik = new BigInteger("0");
        for(int i = 0; i<n*n; i++){
            wynik = wynik.add(BigInteger.valueOf((int)Math.pow(2,i)));
        }
        return  wynik;
    }
}

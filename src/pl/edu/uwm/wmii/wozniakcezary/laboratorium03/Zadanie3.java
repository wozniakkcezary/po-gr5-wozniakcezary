package pl.edu.uwm.wmii.wozniakcezary.laboratorium03;

import java.util.Scanner;
import java.io.File;
import java.io.FileNotFoundException;

public class Zadanie3 {
    public static void main(String[] args) throws FileNotFoundException{
        Scanner in = new Scanner(System.in);
        System.out.println("podaj nazwe pliku (pelen adres razem z rozszerzeniem np. C:\\JavaTest\\test.txt):");
        String fileName = in.nextLine();
        System.out.println("podaj slowo:");
        String subStr = in.nextLine();
        System.out.println("powtarza się "+countSubStr(fileName,subStr)+" razy");
    }

    public static int countSubStr(String fileName,String subStr) {
        try {
            Scanner file = new Scanner(new File(fileName));
            int wynik = 0;
            while (file.hasNextLine()) {
                String line = file.nextLine();
                for(int i=0; i<line.length()-subStr.length()+1; i++){
                    if(subStr.equals(line.substring(i,i+subStr.length()))) {
                        wynik++;
                    }
                }
            }
            return wynik;
        }
        catch (FileNotFoundException ex){
            System.out.println("nie znaleziono pliku");
            return 0;
        }


    }
}

package pl.edu.uwm.wmii.wozniakcezary.laboratorium03;

import java.util.Scanner;

public class Zadanie1 {
    public static void main(String[] args) {
        checkNiceVar();
    }

    public static void checkCountChar() {
        Scanner in = new Scanner(System.in);
        System.out.println("podaj str:");
        String str = in.nextLine();
        System.out.println("podaj c:");
        char c = in.next().charAt(0);
        int a = countChar(str, c);
        System.out.println(c + " powtarza sie w " + str + " " + a + " razy");
    }

    public static int countCharA(String str, char c) {
        int n = str.length();
        int wynik = 0;
        char[] strA = str.toCharArray();
        for (int i = 0; i < n; i++) {
            if (strA[i] == c) {
                wynik++;
            }
        }
        return wynik;
    }

    public static int countChar(String str, char c){
        int wynik = 0;
        for (int i = 0; i < str.length(); i++){
            if(str.charAt(i)==c) {
                wynik++;
            }
        }
        return wynik;
    }

    public static void checkCountSubStr() {
        Scanner in = new Scanner(System.in);
        System.out.println("podaj str:");
        String str = in.nextLine();
        System.out.println("podaj subStr:");
        String subStr = in.nextLine();
        int a = countSubStr(str, subStr);
        System.out.println(subStr + " powtarza sie w " + str + " " + a + " razy");
    }

    public static int countSubStrA(String str, String subStr) {
        int n = str.length();
        int m = subStr.length();
        int wynik = 0;
        int temp = 0;
        char[] strA = str.toCharArray();
        char[] subStrA = subStr.toCharArray();
        for (int i = 0; i < n; i++) {
            if (strA[i] == subStrA[temp]) {
                temp++;
                if (temp == m) {
                    wynik++;
                    temp = 0;
                }
            } else {
                if (strA[i] == subStrA[0]) {
                    temp = 1;
                } else {
                    temp = 0;
                }
            }
        }
        return wynik;
    }

    public static int countSubStr(String str, String subStr){
        int wynik=0;
        for(int i=0; i<str.length()-subStr.length()+1; i++){
            if(subStr.equals(str.substring(i,i+subStr.length()))) {
                wynik++;
            }
        }
        return wynik;
    }

    public static void checkMiddle() {
        Scanner in = new Scanner(System.in);
        System.out.println("podaj str:");
        String str = in.nextLine();
        System.out.println("srodek " + str + " to " + middle(str));
    }

    public static String middleA(String str) {
        int n = str.length();
        String wynik = "";
        char[] strA = str.toCharArray();
        if (n % 2 == 0) {
            wynik = wynik + strA[(n / 2) - 1] + strA[n / 2];
        } else {
            wynik = wynik + strA[n / 2];
        }
        return wynik;
    }

    public static String middle(String str) {
        if(str.length()%2==0){
            return str.substring((str.length()/2)-1,(str.length()/2)+1);
        }
        else{
            return str.substring(str.length()/2,(str.length()/2)+1);
        }
    }

    public static void checkRepeat() {
        Scanner in = new Scanner(System.in);
        System.out.println("podaj str:");
        String str = in.nextLine();
        System.out.println("podaj n:");
        int n = in.nextInt();
        System.out.println(str + " razy " + n + " to " + repeat(str, n));
    }

    public static String repeat(String str, int n) {
        String wynik = str;
        for (int i = 1; i < n; i++) {
            wynik = wynik + str;
        }
        return wynik;
    }

    public static void checkWhere() {
        Scanner in = new Scanner(System.in);
        System.out.println("podaj str:");
        String str = in.nextLine();
        System.out.println("podaj subStr:");
        String subStr = in.nextLine();
        int[] a = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        a = where(str, subStr);
        System.out.println("w " + str + " string " + subStr + " wystepuje na pozycjach: ");
        for (int x : a) {
            if (x != 0) {
                System.out.println(x);
            }
        }
    }

    public static int[] whereA(String str, String subStr) {
        int temp = 0;
        int ind = 0;
        int[] wynik = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        for (int i = 0; i < str.length(); i++) {
            if (str.charAt(i) == subStr.charAt(temp)) {
                temp++;
                if (temp == subStr.length()) {
                    wynik[ind] = i - temp + 2;
                    ind++;
                    temp = 0;
                }
            } else {
                if (str.charAt(i) == subStr.charAt(0)) {
                    temp = 1;
                } else {
                    temp = 0;
                }
            }
        }
        return wynik;
    }

    public static int[] where(String str, String subStr){
        int n = countSubStr(str,subStr);
        int[] wynik = new int[n];
        int ind = 0;
        for(int i=0; i<str.length()-subStr.length()+1; i++){
            if(subStr.equals(str.substring(i,i+subStr.length()))) {
                wynik[ind]=i+1;
                ind++;
            }
        }
        return wynik;
    }

    public static void checkChange() {
        System.out.println("podaj str: ");
        Scanner in = new Scanner(System.in);
        String str = in.nextLine();
        System.out.println(change(str));
    }

    public static String change(String str) {
        StringBuffer msg = new StringBuffer();
        msg.append(str);
        for (int i = 0; i < msg.length(); i++) {
            if (Character.isUpperCase(msg.charAt(i))) {
                msg.setCharAt(i, Character.toLowerCase(msg.charAt(i)));
            } else if (Character.isLowerCase(msg.charAt(i))) {
                msg.setCharAt(i, Character.toUpperCase(msg.charAt(i)));
            }
        }
        return msg.toString();
    }

    public static void checkNice(){
        System.out.println("podaj str: ");
        Scanner in = new Scanner(System.in);
        String str = in.nextLine();
        System.out.println(nice(str));
    }

    public static String nice(String str){
        StringBuffer msg = new StringBuffer();
        msg.append(str);
        int n=msg.length();
        int temp = 3-msg.length()%3;
        for (int i = 1; i<=(n-1)/3 ;i++){
            msg.insert(n-3*i,",");
        }
        return msg.toString();
    }

    public static void checkNiceVar(){
        System.out.println("podaj str: ");
        Scanner in = new Scanner(System.in);
        String str = in.nextLine();
        System.out.println("podaj a: ");
        int a = in.nextInt();
        System.out.println("podaj char: ");
        char c = in.next().charAt(0);
        System.out.println(niceVar(str,a,c));
    }

    public static String niceVar(String str, int a, char c){
        StringBuffer msg = new StringBuffer();
        msg.append(str);
        String s =""+c;
        int n=msg.length();
        int temp = a-msg.length()%a;
        for (int i = 1; i<=(n-1)/a ;i++){
            msg.insert(n-a*i,s);
        }
        return msg.toString();
    }
}
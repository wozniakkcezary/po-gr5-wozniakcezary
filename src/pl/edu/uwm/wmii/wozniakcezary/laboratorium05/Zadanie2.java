package pl.edu.uwm.wmii.wozniakcezary.laboratorium05;

class IntegerSet{
    IntegerSet(){
        tab = new boolean[100];
    }
    public void insertElement(int x){
        if( 1 <= x && x <=100)
            tab[x - 1] = true;
    }
    public void deleteElement(int x){
        if(1 <=x && x <100)
            tab[x -1] = false;
    }
    public static IntegerSet union (IntegerSet is, IntegerSet ins){
        IntegerSet res = new IntegerSet();
        for(int i =1 ; i < 101; i++){
            if(is.tab[i -1] == true || ins.tab[i -1] == true)
                res.tab[i -1] = true;
        }
        return res;
    }
    public static IntegerSet intersection(IntegerSet is, IntegerSet ins){
        IntegerSet res = new IntegerSet();
        for(int i=0; i < 100; i++){
            if(is.tab[i -1] == true && ins.tab[i -1])
                res.tab[i -1] = true;
        }
        return res;
    }
    public String toString(){
        String res = "";
        for(int i =1; i< 101; i++){
            if(tab[i -1] == true)
                res += i + " ";
        }
        return res;
    }

    public boolean equals(IntegerSet is) {
        for(int i =1 ; i < 101; i++){
            if(this.tab[i - 1] != is.tab[i- 1])
                return false;
        }
        return true;
    }

    boolean [] tab;
}
public class Zadanie2 {
    public static void main(String [] args){

        IntegerSet is = new IntegerSet();

        // is.insertElement(99);
        is.insertElement(19);
        is.insertElement(12);

        System.out.println(is.toString());

        IntegerSet kor = new IntegerSet();

        kor.insertElement(12);
        kor.insertElement(19);

        System.out.println(kor.equals(is));

        IntegerSet c = IntegerSet.union(is, kor);
        // System.out.println(c);
    }
}
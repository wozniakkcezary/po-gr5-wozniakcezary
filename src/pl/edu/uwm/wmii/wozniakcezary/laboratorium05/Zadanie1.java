package pl.edu.uwm.wmii.wozniakcezary.laboratorium05;

class RachunekBankowy{
    RachunekBankowy(double saldo){
        this.saldo = saldo;
        rocznaStopaProcentowa = .1;
    }


    double obliczMiesieczneOdsetki(){
        double odsetki = (saldo * rocznaStopaProcentowa) / 12;
        return saldo += odsetki;
    }
    public static void setRocznaStopaProcentowa(double stopa){
        rocznaStopaProcentowa = stopa;
    }
    public double getSaldo(){
        return saldo;
    }
    private double saldo;
    static double rocznaStopaProcentowa;
}
public class Zadanie1 {
    public static void main(String []args){

        RachunekBankowy saver1 = new RachunekBankowy(2000);
        RachunekBankowy saver2 = new RachunekBankowy(3000);
        RachunekBankowy.setRocznaStopaProcentowa(0.04);

        saver1.obliczMiesieczneOdsetki();
        saver2.obliczMiesieczneOdsetki();

        System.out.println(saver1.getSaldo());
        System.out.println(saver2.getSaldo());

        RachunekBankowy.setRocznaStopaProcentowa(.05);

        saver1.obliczMiesieczneOdsetki();
        saver2.obliczMiesieczneOdsetki();

        System.out.println(saver1.getSaldo());
        System.out.println(saver2.getSaldo());

    }
}